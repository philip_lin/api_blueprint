
# Group ACCOUNT 

## account001 Create User 新增帳號 [POST /api/v1/account/user/add]

<br>

### Request

- Auth Required

| Parameter        | Type      | Description                                       |
| ---------------- | --------- | ------------------------------------------------- |
| email            | String    | *密碼收件信箱, 登入帳號                           |
| biz_code         | string    | *系統代碼                                         |
| username         | String    | *使用者名稱                                       |
| user_type        | String    | *使用者種類                                       |
| first_name       | String    | *名                                               |
| last_name        | String    | *姓                                               |
| title            | String    | 職稱                                              |
| phone            | String    | 電話                                              |
| pswd_expiring_at | timestamp | 密碼過期時間                                      |
| adm_role_ids     | Long[]    | *角色識別碼列表                                   |
| supplier_id      | String    | 供應商識別碼 <font color="red">ESG專屬欄位</font> |
| product_category | String[]  | 產品種類 <font color="red">ESG專屬欄位</font>     |
  
<br>

### Response

| Parameter | Type                   | Description |
| --------- | ---------------------- | ----------- |
| user      | <font color="blue"><[USER](#header-user)></font> |             |

<br>

### Response Error Codes

- A0001:帳號重複

<br>

### Logic

| table         | Description |
| ------------- | ----------- |
| adm_user      |             |
| adm_user_role |             |

<br>

1. 檢查email帳號是否重複。
2. 檢查各欄位的值。
3. 檢查是否有角色role_id?有才寫adm_user_role table。
4. 做密碼相關欄位初始值
   - is_initial_pswd = 1
   - is_activated = 0
   - user_status = 0
5. 將相關新增欄位寫入adm_user table
6. 發送email密碼信
7. <font color="red">公用程式先不發信, 先預設寫入一組random密碼</font>

<br>

+ Request (application/json)
    + Body

            {
                "email": "test@aceraeb.com",
                "biz_code": "admin",
                "username": "test",
                "user_type": "acer",
                "first_name": "Lin",
                "last_name":"Wei-Chen",
                "title":"user",
                "phone":"02-2696-1234",
                "pswd_expiring_at": "2022/03/23 15:00:00",
                "adm_role_ids":["00000001","00000003"],
                "supplier_id": "",
                "product_category": ["CPU","HDD"],
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "user": {
                        "entity_id": "00000001",
                        "email": "test@aceraeb.com",
                        "biz_code": "admin",
                        "username": "test",
                        "user_type": "acer",
                        "first_name": "Lin",
                        "last_name":"Wei-Chen",
                        "title":"user",
                        "phone":"02-2696-1234",
                        "pswd_expiring_at": "2022/03/23 15:00:00",
                        "user_status":"0",
                        "role_list":[
                            {
                                "entity_id": "00000001",
                                "biz_code": "admin",
                                "role_name": "ARSM Admin",
                                "is_enabled": "1",
                                "created_at": "2022/03/28 15:00:00",
                                "updated_at": "2022/03/28 15:00:00",
                            },
                        ]
                        "supplier_id": "",
                        "product_category": ["CPU","HDD"],
                        "created_id": "00000001",
                        "created_at": "2022/03/23 15:00:00"
                    }
                    
                }
            }



## account002 Edit User 修改帳號 [POST /api/v1/account/user/edit]

<br>

### Request

- Auth Required

| Parameter        | Type      | Description                                       |
| ---------------- | --------- | ------------------------------------------------- |
| entity_id        | Long      | *使用者識別碼                                     |
| biz_code         | string    | 系統代碼                                          |
| username         | String    | 使用者名稱                                        |
| user_type        | String    | 使用者種類                                        |
| first_name       | String    | 名                                                |
| last_name        | String    | 姓                                                |
| title            | String    | 職稱                                              |
| phone            | String    | 電話                                              |
| pswd_expiring_at | timestamp | 密碼過期時間                                      |
| user_status      | String    | 帳號狀態                                          |
| adm_role_ids     | Long[]    | 角色識別碼列表                                    |
| supplier_id      | String    | 供應商識別碼 <font color="red">ESG專屬欄位</font> |
| product_category | String[]  | 產品種類 <font color="red">ESG專屬欄位</font>     |

<br>

### Response

| Parameter | Type                   | Description |
| --------- | ---------------------- | ----------- |
| user      | <font color="blue"><[USER](#header-user)></font> |             |

<br>

### Response Error Codes

- A0001: 帳號重複

<br>

### Logic

| table         | Description |
| ------------- | ----------- |
| adm_user      |             |
| adm_user_role |             |


1. 判斷修改角色為誰。
2. 如修改email需檢查是否有重複。
3. 檢查是否更換角色role_list?有才寫adm_user_role table。
4. 將相關新增欄位寫入adm_user table
   - updated_adm_user_id = 登入者

<br>

+ Request (application/json)
    + Body

            {
                "entity_id": "00000001",
                "email": "test@aceraeb.com",
                "biz_code": "admin",
                "username": "test",
                "user_type": "acer",
                "first_name": "Lin",
                "last_name":"Wei-Chen",
                "title":"user",
                "phone":"02-2696-1234",
                "pswd_expiring_at": "2022/03/23 15:00:00",
                "adm_role_ids":["00000001","00000003"],
                "supplier_id": "",
                "product_category": ["CPU","HDD"],
            }
            
+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "user": {
                        "entity_id": "00000001",
                        "email": "test@aceraeb.com",
                        "biz_code": "admin",
                        "username": "test",
                        "user_type": "acer",
                        "first_name": "Lin",
                        "last_name":"Wei-Chen",
                        "title":"user",
                        "phone":"02-2696-1234",
                        "pswd_expiring_at": "2022/03/23 15:00:00",
                        "user_status":"0",
                        "role_list":[
                            {
                                "entity_id": "00000001",
                                "biz_code": "admin",
                                "role_name": "ARSM Admin",
                                "is_enabled": "1",
                                "created_at": "2022/03/28 15:00:00",
                                "updated_at": "2022/03/28 15:00:00",
                            },
                        ]
                        "supplier_id": "",
                        "product_category": ["CPU","HDD"],
                        "created_id": "00000001",
                        "created_at": "2022/03/23 15:00:00"
                    }
                }
            }



## account003 Delete User 刪除帳號 [POST /api/v1/account/user/delete]

<br>

### Request

- Auth Required

| Parameter | Type | Description                       |
| --------- | ---- | --------------------------------- |
| entity_id | Long | *使用者識別碼(adm_user.entity_id) |

<br>

### Response



| Parameter | Type | Description |
| --------- | ---- | ----------- |
| (N/A)     |      |             |


<br>


### Response Error Codes

- A0002 找不到此帳號

<br>

### Logic

| table         | Description |
| ------------- | ----------- |
| adm_user      |             |
| adm_user_role |             |


1. 判斷修改角色為誰。
2. 檢查權限是否可以刪除帳號。
3. 將user_status = 3
   - updated_adm_user_id = 登入者
4. 更新adm_user table

<br>
+ Request (application/json)
    + Body

            {
                "entity_id": 10001
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    
                }
            }

## account004  User Profile 取得帳號資訊 [POST /api/v1/account/user/get]

<br>

### Request

- Auth Required

| Parameter | Type | Description   |
| --------- | ---- | ------------- |
| entity_id | Long | *使用者識別碼 |

<br>

### Response

| Parameter | Type                   | Description |
| --------- | ---------------------- | ----------- |
| user      | <font color="blue"><[USER](#header-user)></font> |             |

<br>

### Response Error Codes

- A0002: 找不到此帳號


### Logic


| table         | Description |
| ------------- | ----------- |
| adm_user      |             |
| adm_user_role |             |

1. 判斷登入者的角色與此ID關係

<br>


+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "user": {
                        "entity_id": "00000001",
                        "email": "test@aceraeb.com",
                        "biz_code": "admin",
                        "username": "test",
                        "user_type": "acer",
                        "first_name": "Lin",
                        "last_name":"Wei-Chen",
                        "title":"user",
                        "phone":"02-2696-1234",
                        "pswd_expiring_at": "2022/03/23 15:00:00",
                        "user_status":"0",
                        "role_list":[
                            {
                                "entity_id": "00000001",
                                "biz_code": "admin",
                                "role_name": "ARSM Admin",
                                "is_enabled": "1",
                                "created_at": "2022/03/28 15:00:00",
                                "updated_at": "2022/03/28 15:00:00",
                            },
                        ]
                        "supplier_id": "",
                        "product_category": ["CPU","HDD"],
                        "created_id": "00000001",
                        "created_at": "2022/03/23 15:00:00"
                    }
                }
            }

## account005  User List 取得帳號清單 [POST /api/v1/account/user/search]

<br>

### Request

- Auth Required

| Parameter   | Type   | Description |
| ----------- | ------ | ----------- |
| user_status | String | 使用者狀態  |


<br>

### Response

| Parameter | Type                     | Description |
| --------- | ------------------------ | ----------- |
| user_list | <font color="blue"><[USER](#header-user)>[]</font> |             |

<br>

### Response Error Codes

N/A

### Logic

| table         | Description |
| ------------- | ----------- |
| adm_user      |             |
| adm_user_role |             |


1. 判斷角色範圍。
2. 取得篩選條件範圍 

<br>

+ Request (application/json)
    + Body

            {
                "user_status":"0",
            } 

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "user_list":[
                        {
                            "entity_id": "00000001",
                            "email": "test@aceraeb.com",
                            "biz_code": "admin",
                            "username": "test",
                            "user_type": "acer",
                            "first_name": "Lin",
                            "last_name":"Wei-Chen",
                            "title":"user",
                            "phone":"02-2696-1234",
                            "pswd_expiring_at": "2022/03/23 15:00:00",
                            "user_status":"0",
                            "role_list":[
                                {
                                    "entity_id": "00000001",
                                    "biz_code": "admin",
                                    "role_name": "ARSM Admin",
                                    "is_enabled": "1",
                                    "created_at": "2022/03/28 15:00:00",
                                    "updated_at": "2022/03/28 15:00:00",
                                },
                            ]
                            "supplier_id": "",
                            "product_category": ["CPU","HDD"],
                            "created_id": "00000001",
                            "created_at": "2022/03/23 15:00:00"
                        }
                    ]
                }
            }



## account006  Login 登入 [POST /api/v1/account/login]

<br>

### Request

- Auth Not Required

| Parameter | Type   | Description |
| --------- | ------ | ----------- |
| email     | String | *登入帳號   |
| pswd      | String | *密碼       |

<br>

### Response

| Parameter | Type                 | Description |
| --------- | -------------------- | ----------- |
| user      | <font color="blue">[USER](#header-user)</font> |             |


<br>

### Response Error Codes

- A0002: 找不到此帳號
- A0003: 帳號狀態錯誤
- A0004: 登入失敗(不顯示:密碼錯誤)
- A0005: 帳號密碼過期
- A0006: 帳號未驗證

<br>

### Logic

| table              | Description |
| ------------------ | ----------- |
| adm_user           |             |
| adm_user_role      |             |
| adm_user_login_log |             |

1. 透過biz_code加email檢查帳號是否存在。
   - 找不到回user0002 找不到此帳號
2. 檢查是否已驗證is_activated = 1
   - 未驗證時回user0006 帳號未驗證
3. 檢查密碼、是否為初始密碼以及密碼是否過期
   - is_initial_pswd、pswd_expiring_at
   - 錯誤則回user0004 登入失敗(不顯示:密碼錯誤)
4. 檢查帳號狀態 user_status = 0 不等於0都回user0003 帳號狀態錯誤
5. 取得此帳號資訊以及角色功能表內容
6. 更新此帳號的登入資訊以及log。
7. 登入失敗須更新
   - login_fail_amt + 1 

<br>


+ Request (application/json)
    + Body

            {
                "email": "test3@aceraeb.com",
                "pswd": "test2",
            }


+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "user": {
                        "entity_id": "00000001",
                        "email": "test@aceraeb.com",
                        "biz_code": "admin",
                        "username": "test",
                        "user_type": "acer",
                        "first_name": "Lin",
                        "last_name":"Wei-Chen",
                        "title":"user",
                        "phone":"02-2696-1234",
                        "pswd_expiring_at": "2022/03/23 15:00:00",
                        "user_status":"0",
                        "role_list":[
                            {
                                "entity_id": "00000001",
                                "biz_code": "admin",
                                "role_name": "ARSM Admin",
                                "is_enabled": "1",
                                "created_at": "2022/03/28 15:00:00",
                                "updated_at": "2022/03/28 15:00:00",
                            },
                        ]
                        "supplier_id": "",
                        "product_category": ["CPU","HDD"],
                        "created_id": "00000001",
                        "created_at": "2022/03/23 15:00:00"
                    }
                }
            }



## account007  Forgot Password 忘記密碼 [POST /api/v1/account/forgetpswd]

<br>

### Request

- Auth Not Required

| Parameter | Type   | Description |
| --------- | ------ | ----------- |
| email     | String | *登入帳號   |

<br>

### Response

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| (N/A)     |      |             |

<br>

### Response Error Codes

- A0002: 找不到此帳號

<br>

### Logic

| table    | Description |
| -------- | ----------- |
| adm_user |             |

1. 檢查帳號是否存在。

<br>

+ Request (application/json)
    + Body

            {
                "email": "test3@aceraeb.com",
            }
            
+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    
                }
            }



## account008  Reset password 重置密碼 [POST /api/v1/account/resetpswd]

<br>

### Request

- Auth Not Required



| Parameter    | Type   | Description   |
| ------------ | ------ | ------------- |
| token        | String | *             |
| pswd         | String | *修改密碼     |
| confirm_pswd | String | *確認修改密碼 |

<br>

### Response

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| (N/A)     |      |             |

<br>

### Response Error Codes

- A0002: 找不到此帳號

<br>

### Logic

| table    | Description |
| -------- | ----------- |
| adm_user |             |


1. 檢查帳號是否存在。

<br>

+ Request (application/json)
    + Body

            {
                "token": "jfdiogou398475496",
                "pswd": "********",
                "confirm_pswd": "********"
            }
            
+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    
                }
            }



## account009  Activate Account 啟動帳號 [POST /api/v1/account/activate]

<br>

### Request

- Auth Not Required

| Parameter | Type   | Description |
| --------- | ------ | ----------- |
| token     | String |             |


<br>

### Response

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| (N/A)     |      |             |

<br>

### Response Error Codes

- A0002: 找不到此帳號

<br>


### Logic

| table    | Description |
| -------- | ----------- |
| adm_user |             |

1. 檢查token正確性以及效期。

            
+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    
                }
            }


## account010  User history 取得帳號歷史紀錄 [POST /api/v1/account/user/history]

<font color="red">公版不用處理</font>

<br>

### Request

- Auth Required

| Parameter   | Type | Description   |
| ----------- | ---- | ------------- |
| adm_user_id | Long | *使用者識別碼 |

<br>

### Response

| Parameter             | Type                                     | Description |
| --------------------- | ---------------------------------------- | ----------- |
| adm_user_histoty_list | <font color="blue"><[User_History](#header-user_history)>[]</font> |             |


<br>

### Response Error Codes

- A0002 找不到此帳號

### Logic

| table               | Description |
| ------------------- | ----------- |
| adm_user            |             |
| adm_user_role       |             |
| adm_user_action_log |             |


1. 判斷修改角色為誰。

<br>

+ Request (application/json)
    + Body

            {
                "adm_user_id": "00000001",
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "adm_user_histoty_list":[
                        {
                            "adm_user_action_log_id": "00000001",
                            "adm_user_id": "00000001",
                            "username": "test2",
                            "category": "adm_user",
                            "action_type": "update",
                            "data": "username: test2",
                            "created_at":"2022/03/30 15:00:00",
                            "updated_at":"2022/03/30 15:00:00"
                        },
                        {
                            "adm_user_action_log_id": "00000003",
                            "adm_user_id": "00000001",
                            "username": "test3",
                            "category": "adm_user",
                            "action_type": "update",
                            "data": "username: test3",
                            "created_at":"2022/03/30 15:00:00",
                            "updated_at":"2022/03/30 15:00:00"
                        }
                    ]
                    
                }
            }



## account100 取得角色清單  [POST /api/v1/account/role/search]

<br>

### Request

- Auth Required

| Parameter  | Type    | Description |
| ---------- | ------- | ----------- |
| is_enabled | Boolean | 是否啟用    |

<br>

### Response

| Parameter | Type                     | Description |
| --------- | ------------------------ | ----------- |
| role_list | <font color="blue"><[ROLE](#header-role)>[]</font> |             |

<br>

### Response Error Codes

- A0100:無角色權限

<br>

### Logic

| table        | Description |
| ------------ | ----------- |
| adm_role     |             |
| adm_role_acl |             |

1. 檢查此帳號是否有權限查看。

<br>

+ Request (application/json)
    + Body

            {
                "is_enabled": "1"
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "role_list":[
                        {
                            "entity_id": "00000001",
                            "biz_code": "admin",
                            "role_name": "ARSM Admin",
                            "is_enabled": "1",
                            "acl_list":[
                                {
                                    "entity_id",
                                    "biz_code",
                                    "acl_code",
                                    "acl_type",
                                    "acl_name",
                                    "lv",
                                    "parent_acl_code",
                                    "to_url",
                                    "memo",
                                    "sort_no",
                                    "is_enabled",
                                    "created_at": "2022/03/28 15:00:00",
                                    "updated_at": "2022/03/28 15:00:00",
                                }
                            ],
                            "created_at": "2022/03/28 15:00:00",
                            "updated_at": "2022/03/28 15:00:00",
                        },
                    ]
                }
            }


## account101 新增角色  [POST /api/v1/account/role/add]

<br>

### Request

- Auth Required

| Parameter     | Type     | Description    |
| ------------- | -------- | -------------- |
| biz_code      | string   | 系統代碼       |
| role_name     | String   | 角色名稱       |
| is_enabled    | Boolean  | 是否啟用       |
| adm_acl_codes | string[] | *ACL識別碼列表 |

<br>

### Response

| Parameter | Type                   | Description |
| --------- | ---------------------- | ----------- |
| role      | <font color="blue"><[ROLE](#header-role)></font> |             |

<br>

### Response Error Codes

- A0100:無角色權限
- A0101:角色名稱重複

<br>

### Logic


| table        | Description |
| ------------ | ----------- |
| adm_role     |             |
| adm_role_acl |             |

1. 檢查此帳號是否有權限新增。
2. 檢查role_name是否有重複。

<br>

+ Request (application/json)
    + Body

            {
                "biz_code": "admin",
                "role_name": "PUR manager",
                "is_enabled": "1",
                "adm_acl_codes": ["acl","role"],
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "role":{
                        "entity_id": "00000001",
                        "biz_code": "admin",
                        "role_name": "ARSM Admin",
                        "is_enabled": "1",
                        "acl_list":[
                            {
                                "entity_id",
                                "biz_code",
                                "acl_code",
                                "acl_type",
                                "acl_name",
                                "lv",
                                "parent_acl_code",
                                "to_url",
                                "memo",
                                "sort_no",
                                "is_enabled",
                                "created_at": "2022/03/28 15:00:00",
                                "updated_at": "2022/03/28 15:00:00",
                            }
                        ],
                        "created_at": "2022/03/28 15:00:00",
                        "updated_at": "2022/03/28 15:00:00",
                        
                    },
                }
            }




## account102 修改角色  [POST /api/v1/account/role/edit]

<br>

### Request

- Auth Required

| Parameter     | Type     | Description    |
| ------------- | -------- | -------------- |
| entity_id     | Long     | *角色識別碼    |
| biz_code      | string   | 系統代碼       |
| role_name     | String   | 角色名稱       |
| is_enabled    | Boolean  | 是否啟用       |
| adm_acl_codes | string[] | *ACL識別碼列表 |

<br>

### Response

| Parameter | Type                   | Description |
| --------- | ---------------------- | ----------- |
| role      | <font color="blue"><[ROLE](#header-role)></font> |             |

<br>

### Response Error Codes

- A0100:無角色權限
- A0101:角色名稱重複

<br>

### Logic


| table        | Description |
| ------------ | ----------- |
| adm_role     |             |
| adm_role_acl |             |

1. 檢查此帳號是否有權限修改。
2. 檢查角色名稱是否重複。

<br>

+ Request (application/json)
    + Body

            {
                "entity_id": "00000001",
                "biz_code": "admin",
                "role_name": "PUR manager",
                "is_enabled": "1",
                "adm_acl_codes": ["acl","role"],
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "role":{
                        "entity_id": "00000001",
                        "biz_code": "admin",
                        "role_name": "ARSM Admin",
                        "is_enabled": "1",
                        "acl_list":[
                            {
                                "entity_id",
                                "biz_code",
                                "acl_code",
                                "acl_type",
                                "acl_name",
                                "lv",
                                "parent_acl_code",
                                "to_url",
                                "memo",
                                "sort_no",
                                "is_enabled",
                                "created_at": "2022/03/28 15:00:00",
                                "updated_at": "2022/03/28 15:00:00",
                            }
                        ],
                        "created_at": "2022/03/28 15:00:00",
                        "updated_at": "2022/03/28 15:00:00",
                        
                    },
                }
            }



## account103 刪除角色  [POST /api/v1/account/role/delete]

<br>

### Request

- Auth Required

| Parameter | Type | Description        |
| --------- | ---- | ------------------ |
| entity_id | Long | adm_role.entity_id |

<br>

### Response

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| (N/A)     |      |             |

<br>

### Response Error Codes

- A0100:無角色權限

<br>

### Logic

| table        | Description |
| ------------ | ----------- |
| adm_role     |             |
| adm_role_acl |             |

1. 檢查此帳號是否有權限刪除。

<br>

+ Request (application/json)
    + Body

            {
                "entity_id": 10001
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    
                }
            }


## account104 取得單一角色  [POST /api/v1/account/role/get]

<br>

### Request

- Auth Required

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| entity_id | Long | *角色識別碼 |


<br>

### Response

| Parameter | Type                   | Description |
| --------- | ---------------------- | ----------- |
| role      | <font color="blue"><[ROLE](#header-role)></font> |             |

<br>

### Response Error Codes

- A0100:無角色權限

<br>

### Logic


| table        | Description |
| ------------ | ----------- |
| adm_role     |             |
| adm_role_acl |             |



<br>

+ Request (application/json)
    + Body

            {
                "entity_id": "00000001",
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "role":{
                        "entity_id": "00000001",
                        "biz_code": "admin",
                        "role_name": "ARSM Admin",
                        "is_enabled": "1",
                        "acl_list":[
                            {
                                "entity_id",
                                "biz_code",
                                "acl_code",
                                "acl_type",
                                "acl_name",
                                "lv",
                                "parent_acl_code",
                                "to_url",
                                "memo",
                                "sort_no",
                                "is_enabled",
                                "created_at": "2022/03/28 15:00:00",
                                "updated_at": "2022/03/28 15:00:00",
                            }
                        ],
                        "created_at": "2022/03/28 15:00:00",
                        "updated_at": "2022/03/28 15:00:00",
                        
                    },
                }
            }


## account200 取得系統權限項目清單  [POST /api/v1/account/acl/search]

<br>

### Request

- Auth Required

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| (N/A)     |      |             |

<br>

### Response

| Parameter | Type                   | Description |
| --------- | ---------------------- | ----------- |
| acl_list  | <font color="blue"><[ACL](#header-acl)>[]</font> |             |

<br>

### Response Error Codes

- A0200:無此功能頁權限

<br>

### Logic

| table   | Description |
| ------- | ----------- |
| adm_acl |             |

1. 檢查此帳號是否有權限查看。

<br>

+ Request (application/json)
    + Body

            {
                "role_name": "PUR manager"
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "acl_list":[
                        {
                            "entity_id",
                            "biz_code",
                            "acl_code",
                            "acl_type",
                            "acl_name",
                            "lv",
                            "parent_acl_code",
                            "to_url",
                            "memo",
                            "sort_no",
                            "is_enabled",
                            "created_at": "2022/03/28 15:00:00",
                            "updated_at": "2022/03/28 15:00:00",
                        }
                    ]
                }
            }



## account201 新增權限項目  [POST /api/v1/account/acl/add]

<br>

### Request

- Auth Required

| Parameter       | Type    | Description |
| --------------- | ------- | ----------- |
| biz_code        | String  | 系統代碼    |
| acl_code        | String  | ACL代碼     |
| acl_type        | String  | ACL類別     |
| acl_name        | String  | ACL名稱     |
| lv              | String  | 層級        |
| parent_acl_code | String  | 上層ACL代碼 |
| to_url          | String  | 連結網址    |
| memo            | String  | 備註        |
| sort_no         | String  | 排序        |
| is_enabled      | Boolean | 是否啟用    |

<br>

### Response

| Parameter | Type                 | Description |
| --------- | -------------------- | ----------- |
| acl       | <font color="blue"><[ACL](#header-acl)></font> |             |

<br>

### Response Error Codes

- A0200:無此功能頁權限

### Logic

| table   | Description |
| ------- | ----------- |
| adm_acl |             |

1. 檢查此帳號是否有權限新增。

<br>

+ Request (application/json)
    + Body

            {
                "biz_code",
                "acl_code",
                "acl_type",
                "acl_name",
                "lv",
                "parent_acl_code",
                "to_url",
                "memo",
                "sort_no",
                "is_enabled",
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "acl": {
                        "entity_id",
                        "biz_code",
                        "acl_code",
                        "acl_type",
                        "acl_name",
                        "lv",
                        "parent_acl_code",
                        "to_url",
                        "memo",
                        "sort_no",
                        "is_enabled",
                        "created_at": "2022/03/28 15:00:00",
                        "updated_at": "2022/03/28 15:00:00",
                    }
                }
            }



## account202 修改權限項目  [POST /api/v1/account/acl/edit]

<br>

### Request

- Auth Required

| Parameter       | Type    | Description |
| --------------- | ------- | ----------- |
| entity_id       | Long    | Acl識別碼   |
| biz_code        | String  | 系統代碼    |
| acl_code        | String  | ACL代碼     |
| acl_type        | String  | ACL類別     |
| acl_name        | String  | ACL名稱     |
| lv              | String  | 層級        |
| parent_acl_code | String  | 上層ACL代碼 |
| to_url          | String  | 連結網址    |
| memo            | String  | 備註        |
| sort_no         | String  | 排序        |
| is_enabled      | Boolean | 是否啟用    |

<br>

### Response

| Parameter | Type                 | Description |
| --------- | -------------------- | ----------- |
| acl       | <font color="blue"><[ACL](#header-acl)></font> |             |

<br>

### Response Error Codes

- A0200:無此功能頁權限

### Logic

| table   | Description |
| ------- | ----------- |
| adm_acl |             |

1. 檢查此帳號是否有權限新增。

<br>

+ Request (application/json)
    + Body

            {
                "entity_id",
                "biz_code",
                "acl_code",
                "acl_type",
                "acl_name",
                "lv",
                "parent_acl_code",
                "to_url",
                "memo",
                "sort_no",
                "is_enabled",
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "acl": {
                        "entity_id",
                        "biz_code",
                        "acl_code",
                        "acl_type",
                        "acl_name",
                        "lv",
                        "parent_acl_code",
                        "to_url",
                        "memo",
                        "sort_no",
                        "is_enabled",
                        "created_at": "2022/03/28 15:00:00",
                        "updated_at": "2022/03/28 15:00:00",
                    }
                }
            }



## account203 刪除權限項目  [POST /api/v1/account/acl/detete]

<br>

### Request

- Auth Required

| Parameter | Type | Description       |
| --------- | ---- | ----------------- |
| entity_id | Long | adm_acl.entity_id |

<br>

### Response

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| (N/A)     |      |             |

<br>

### Response Error Codes

- A0200:無此功能頁權限

<br>

### Logic

| table   | Description |
| ------- | ----------- |
| adm_acl |             |

1. 檢查此帳號是否有權限刪除。

<br>

+ Request (application/json)
    + Body

            {
                "entity_id": 10001
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    
                }
            }

## account204 取得單一權限項目  [POST /api/v1/account/acl/get]

<br>

### Request

- Auth Required

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| entity_id | Long | Acl識別碼   |


<br>

### Response

| Parameter | Type                 | Description |
| --------- | -------------------- | ----------- |
| acl       | <font color="blue"><[ACL](#header-acl)></font> |             |

<br>

### Response Error Codes

- A0200:無此功能頁權限

### Logic

| table   | Description |
| ------- | ----------- |
| adm_acl |             |



<br>

+ Request (application/json)
    + Body

            {
                "entity_id",
            }

+ Response 200 (application/json)
    + Body

            {
                "header": {
                    "status": "OK"
                },
                "body": {
                    "acl": {
                        "entity_id",
                        "biz_code",
                        "acl_code",
                        "acl_type",
                        "acl_name",
                        "lv",
                        "parent_acl_code",
                        "to_url",
                        "memo",
                        "sort_no",
                        "is_enabled",
                        "created_at": "2022/03/28 15:00:00",
                        "updated_at": "2022/03/28 15:00:00",
                    }
                }
            }
