

# Group Model - Account

## USER

| Parameter        | Type                     | Description                                       |
| ---------------- | ------------------------ | ------------------------------------------------- |
| entity_id        | String                   | 使用者識別碼                                      |
| email            | String                   | 密碼收件信箱                                      |
| biz_code         | string                   | 系統代碼                                          |
| username         | String                   | 使用者名稱                                        |
| user_type        | String                   | 使用者種類                                        |
| first_name       | String                   | 名                                                |
| last_name        | String                   | 姓                                                |
| title            | String                   | 職稱                                              |
| phone            | String                   | 電話                                              |
| pswd_expiring_at | timestamp                | 密碼過期時間                                      |
| user_status      | String                   | 帳號狀態                                          |
| role_list        | <[ROLE](#header-role)>[] |                                                   |
| created_id       | String                   | 建立者識別碼                                      |
| created_at       | String                   | 建立日期                                          |
| updated_id       | String                   | 更新者識別碼                                      |
| updated_at       | String                   | 更新日期                                          |
| supplier_id      | String                   | 供應商識別碼 <font color="red">ESG專屬欄位</font> |
| product_category | Array                    | 產品種類 <font color="red">ESG專屬欄位</font>     |



## ROLE

| Property   | Type                   | Description |
| ---------- | ---------------------- | ----------- |
| entity_id  | Long                   | 角色識別碼  |
| biz_code   | String                 | 系統代碼    |
| role_name  | String                 | 角色名稱    |
| is_enabled | String                 | 是否啟用    |
| acl_list   | <[ACL](#header-acl)>[] |             |
| created_at | String                 | 建立日期    |
| updated_at | String                 | 異動日期    |


## ACL

| Property        | Type    | Description |
| --------------- | ------- | ----------- |
| entity_id       | Long    | Acl識別碼   |
| biz_code        | String  | 系統代碼    |
| acl_code        | String  | ACL代碼     |
| acl_type        | String  | ACL類別     |
| acl_name        | String  | ACL名稱     |
| lv              | String  | 層級        |
| parent_acl_code | String  | 上層ACL代碼 |
| to_url          | String  | 連結網址    |
| memo            | String  | 備註        |
| sort_no         | String  | 排序        |
| is_enabled      | Boolean | 是否啟用    |
| created_at      | String  | 建立日期    |
| updated_at      | String  | 更新日期    |

## User_History

| Parameter              | Type   | Description        |
| ---------------------- | ------ | ------------------ |
| adm_user_action_log_id | String | 帳號動作紀錄識別碼 |
| adm_user_id            | string | 帳號識別碼         |
| username               | String | 使用者名稱         |
| category               | String | 分類               |
| action_type            | String | 動作型態           |
| data                   | String | 資料               |
| created_at             | String | 建立日期           |
| updated_at             | String | 更新日期           |
